import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

import styles from "./Login.module.css";

const Register = () => {
  const [enteredUsername, setEnteredUsername] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [enteredFullName, setEnteredFullName] = useState("");
  const [enteredPhoneNumber, setEnteredPhoneNumber] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const submitHandle = (event) => {
    event.preventDefault();
    setMessage("");
    if (
      enteredUsername.trim().length === 0 ||
      enteredPassword.trim().length === 0
    ) {
      setMessage("Input not valid");
    } else {
      axios
        .post("http://localhost:5000/register-user", {
          username: enteredUsername,
          password: enteredPassword,
          fullName: enteredFullName,
          phoneNumber: enteredPhoneNumber,
          email: enteredEmail,
        })
        .then((response) => {
          alert(response.data);
          navigate("/login");
        })
        .catch((err) => setMessage(err.response.data));
    }
  };
  const onChangeUsernameHandle = (event) => {
    setEnteredUsername(event.target.value);
  };
  const onChangePasswordHanle = (event) => {
    setEnteredPassword(event.target.value);
  };
  const changeFullNameHandle = (event) => {
    setEnteredFullName(event.target.value);
  };
  const changeEmailHandle = (event) => {
    setEnteredEmail(event.target.value);
  };
  const changePhoneNumberHandle = (event) => {
    setEnteredPhoneNumber(event.target.value);
  };
  return (
    <div className={styles.login}>
      <h2>Sign Up</h2>
      <form className={styles["login-form"]} onSubmit={submitHandle}>
        <p className={styles.message}>{message}</p>
        <input
          placeholder="fullName"
          type="text"
          onChange={changeFullNameHandle}
          value={enteredFullName}
        />
        <input
          placeholder="Email"
          type="email"
          onChange={changeEmailHandle}
          value={enteredEmail}
        />
        <input
          placeholder="Phone Number"
          type="number"
          onChange={changePhoneNumberHandle}
          value={enteredPhoneNumber}
        />
        <input
          placeholder="username"
          onChange={onChangeUsernameHandle}
          type="text"
          value={enteredUsername}
        />
        <input
          placeholder="password"
          onChange={onChangePasswordHanle}
          type="password"
          value={enteredPassword}
        />
        <button type="submit">Create Account</button>
      </form>
    </div>
  );
};

export default Register;
