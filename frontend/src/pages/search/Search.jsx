import SearchContent from "./SearchContent/SearchContent";
import SubscribeForm from "../../components/SubscribeForm/SubscribeForm";
const Search = () => {
  return (
    <div>
      <SearchContent />
      <SubscribeForm />
    </div>
  );
};

export default Search;
