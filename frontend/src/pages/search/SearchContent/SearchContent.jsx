import styles from "./SearchContent.module.css";
import HotelComponentsSearch from "./HotelComponentsSearch";

import { useSelector } from "react-redux";
const SearchContent = () => {
  const searchData = useSelector((state) => state.search.result);
  return (
    <div className={styles["search-content"]}>
      <div className={styles["table-input"]}>
        <div className={styles["title-table-input"]}>
          <h2>Search</h2>
        </div>
        <div className={styles["input-destination"]}>
          <label>Destination</label>
          <input type="text" />
        </div>
        <div className={styles["input-checkin-date"]}>
          <label>Check-in Date</label>
          <input type="text" />
        </div>
        <div className={styles["input-options"]}>
          <h5>Options</h5>
          <div>
            <label>Min price per night</label>
            <input type="number" />
          </div>
          <div>
            <label>Max price per night</label>
            <input type="number" />
          </div>
          <div>
            <label>Adult</label>
            <input type="number" min="0" />
          </div>
          <div>
            <label>Children</label>
            <input type="number" min="0" />
          </div>
          <div>
            <label>Room</label>
            <input type="number" min="0" />
          </div>
        </div>
        <button className={styles["search-btn"]}>Search</button>
      </div>
      <div className={styles["hotels-search"]}>
        {searchData.length > 0 ? (
          searchData.map((el) => {
            return <HotelComponentsSearch dataSearch={el} />;
          })
        ) : (
          <p>0 results</p>
        )}
      </div>
    </div>
  );
};

export default SearchContent;
