import styles from "./HotelComponentsSearch.module.css";

const HotelComponentsSearch = (props) => {
  const freeCancelStyles = {
    color: "#198754",
  };
  return (
    <div className={styles["hotel-components-search"]}>
      <div className="hotel-search-img">
        <img src={props.dataSearch.photos[0]} alt="sHi" />
      </div>
      <div className={styles["description-hotel"]}>
        <h5>{props.dataSearch.name}</h5>
        <p>{`${props.dataSearch.distance}m from center`}</p>
        <div className={styles["tag-hotel"]}>
          {props.dataSearch.featured && "Free airport taxi"}
        </div>
        <div className={styles["hotel-description"]}>
          {props.dataSearch.address}
        </div>
        <p>{props.dataSearch.desc.slice(0, 220)}.....</p>
        <div>
          {props.dataSearch.free_cancel && (
            <div style={freeCancelStyles}>
              <h6>Free cancellation</h6>
              <p>You can cancel later, so lock in this great price today!</p>
            </div>
          )}
        </div>
      </div>
      <div className={styles["section-rate-hotel"]}>
        <div className={styles["rate"]}>
          {/* <h5>{props.dataSearch.rate_text}</h5> */}
          {/* <div>{props.dataSearch.rate}</div> */}
        </div>
        <h2>{`$${props.dataSearch.cheapestPrice}`}</h2>
        <p>includes taxes and fees</p>
        <button className={styles["see-availability-btn"]}>
          See availability
        </button>
      </div>
    </div>
  );
};

export default HotelComponentsSearch;
