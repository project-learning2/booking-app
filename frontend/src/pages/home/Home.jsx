import CityContent from "../../components/ContentHome/CityContent/CityContent";
import ProposeHotel from "../../components/ContentHome/ProposeHotel/ProposeHotel";
import TypeOfHotel from "../../components/ContentHome/TypeOfHotel/TypeOfHotel";
import HeaderHome from "../../components/HeaderHome/HeaderHome";
import SubscribeForm from "../../components/SubscribeForm/SubscribeForm";

const Home = () => {
  return (
    <div>
      <HeaderHome />
      <CityContent />
      <TypeOfHotel />
      <ProposeHotel />
      <SubscribeForm />
    </div>
  );
};

export default Home;
