import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { DateRange } from "react-date-range";
import { useSelector } from "react-redux";

import styles from "./Booking.module.css";
// import { transactionActions } from "../../store/transactions";

const Booking = () => {
  const [selectionRange, setSelectionRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: "selection",
  });
  const userLogin = useSelector((state) => state.auth.userLogin);
  const [data, setData] = useState(null);
  const [roomData, setRoomData] = useState(null);
  const [message, setMessage] = useState(null);
  const [enteredFullName, setEnteredFullName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPhoneNumber, setEnteredPhoneNumber] = useState("");
  const [enteredCardNumber, setEnteredCardNumber] = useState("1");
  const [totalBill, setTotalBill] = useState(0);
  const params = useParams().hotelBook;
  //   const dispatch = useDispatch();
  const navigate = useNavigate();
  const [roomChecked, setRoomChecked] = useState([]);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState("");

  const changeFullNameHandle = (events) => {
    setEnteredFullName(events.target.value);
  };
  const changeEmailHandle = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePhoneNumberHandle = (events) => {
    setEnteredPhoneNumber(events.target.value);
  };
  const changeCardNumbrtHandle = (events) => {
    setEnteredCardNumber(events.target.value);
  };
  useEffect(() => {
    if (!userLogin) {
      navigate("/login");
    } else {
      setEnteredFullName(userLogin.fullName);
      setEnteredEmail(userLogin.email);
      setEnteredPhoneNumber(userLogin.phoneNumber);
    }
  }, [userLogin]);

  useEffect(() => {
    if (enteredFullName.trim().length === 0) {
      setMessage("Entered FullName");
    } else {
      setMessage(null);
    }
  }, [enteredFullName]);
  useEffect(() => {
    if (enteredEmail.trim().length === 0) {
      setMessage("Entered Email");
    } else {
      setMessage(null);
      if (enteredEmail.includes("@")) {
        setMessage("Email not valid");
      }
    }
  }, [enteredEmail]);
  useEffect(() => {
    if (enteredPhoneNumber.trim().length === 0) {
      setMessage("Entered PhoneNumber");
    } else {
      setMessage(null);
    }
  }, [enteredPhoneNumber]);
  useEffect(() => {
    if (enteredCardNumber.trim().length === 0) {
      setMessage("Entered CardNumber");
    } else {
      setMessage(null);
    }
  }, [enteredCardNumber]);

  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-hotel-by-id?hotelId=${params}`)
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, [params, userLogin]);
  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-room-by-id?hotelId=${params}`)
      .then((response) => {
        setRoomData(response.data);
      })
      .catch((err) => console.log(err));
  }, []);
  const clickBookingHandle = () => {};

  const valueCheckChangeHandle = (roomType, room, checked) => {
    setRoomChecked((prevRoomChecked) => {
      const updatedRoomChecked = [...prevRoomChecked];
      let total = 0;
      if (checked) {
        updatedRoomChecked.push({
          room: roomType._id,
          roomNumber: room,
          price: roomType.price,
        });
      } else {
        const roomCheckedIndex = updatedRoomChecked.findIndex((el) => {
          return el.roomTy === room && el.roomNumber === room;
        });
        updatedRoomChecked.splice(roomCheckedIndex, 1);
      }
      updatedRoomChecked.forEach((el) => {
        total += el.price;
      });
      setTotalBill(total);
      return updatedRoomChecked;
    });
    // console.log(roomChecked);
  };

  const handleSelect = (ranges) => {
    setSelectionRange({
      startDate: ranges.selection.startDate,
      endDate: ranges.selection.endDate,
      key: "selection",
    });
  };
  const roomRender =
    roomData &&
    roomData.map((el) => {
      return (
        <div key={el._id} className={styles["room-el"]}>
          <div className={styles["room-inf"]}>
            <h4>{el.title}</h4>
            <p>{el.desc}</p>
            <h6>Max people: {el.maxPeople}</h6>
            <h4>${el.price}</h4>
          </div>
          <div className={styles["room-options"]}>
            {el.roomNumbers.map((room) => {
              return (
                <div key={room} className={styles["room-option"]}>
                  <label>{room}</label>
                  <input
                    type="checkbox"
                    name={room}
                    onChange={(event) =>
                      valueCheckChangeHandle(el, room, event.target.checked)
                    }
                  />
                </div>
              );
            })}
          </div>
        </div>
      );
    });
  const changeSelectPaymentMethodHandle = (events) => {
    setSelectedPaymentMethod(events.target.value);
  };

  const reserveHandle = () => {
    const dataOrder = {
      hotel: params,
      room: roomChecked,
      date: {
        dateStart: selectionRange.startDate,
        dateEnd: selectionRange.endDate,
      },
      price: totalBill,
      paymentMethod: selectedPaymentMethod,
      userId: userLogin._id,
    };
    axios
      .post("http://localhost:5000/post-create-transaction", dataOrder)
      .then((result) => {
        alert(result.data);
        console.log(result.data);
        navigate(`/transactions/${userLogin._id}`);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className={styles.booking}>
      {data && (
        <>
          <div className="footer-details-content">
            <div className="decription-text">
              <h3>{data.title}</h3>
              <p>{data.desc}</p>
            </div>
            <div className="ads-blockk">
              <div className="price-ads">
                <strong>$ {data.cheapestPrice}</strong>
                <span> (1 nights)</span>
              </div>
              <button className="reverse-btn-ads" onClick={clickBookingHandle}>
                Reserve or Book Now!
              </button>
            </div>
          </div>
          <div className={styles["inf-booking"]}>
            <div className={styles["inf-date"]}>
              <h3>Dates</h3>
              <DateRange
                editableDateInputs={true}
                moveRangeOnFirstSelection={true}
                className="date"
                minDate={new Date()}
                onChange={handleSelect}
                ranges={[selectionRange]}
                retainEndDateOnFirstSelection={true}
                showDateDisplay={true}
              />
            </div>
            <div className={styles["inf-form"]}>
              <h3>Reserve Info</h3>
              <div className={message ? styles["err-alert"] : styles["hide"]}>
                {message}
              </div>
              <form className={styles.form}>
                <label>Your Full Name:</label>
                <input
                  placeholder="Full Name"
                  onChange={changeFullNameHandle}
                  type="text"
                  value={enteredFullName}
                />
                <label>Your Email:</label>
                <input
                  placeholder="Email"
                  type="text"
                  onChange={changeEmailHandle}
                  value={enteredEmail}
                />
                <label>Your Phone Number:</label>
                <input
                  placeholder="Phone Number"
                  type="number"
                  value={enteredPhoneNumber}
                  onChange={changePhoneNumberHandle}
                />
                <label>Your Identity Card Number:</label>
                <input
                  placeholder="Card Number"
                  type="number"
                  value={enteredCardNumber}
                  onChange={changeCardNumbrtHandle}
                />
              </form>
            </div>
          </div>
          <div className={styles["select-rooms"]}>
            <h3>Select Rooms</h3>
            <div className={styles.rooms}>{roomRender}</div>
          </div>
          <div className={styles["total-bill"]}>
            <h3>Total Bill: ${totalBill}</h3>
            <select
              value={selectedPaymentMethod}
              onChange={changeSelectPaymentMethodHandle}
            >
              <option value="">Select Payment Method</option>
              <option value="Cash">Cash</option>
              <option value="Credit/Debit">Credit/Debit</option>
            </select>
            <button onClick={reserveHandle}>Reserve Now</button>
          </div>
        </>
      )}
    </div>
  );
};

export default Booking;
