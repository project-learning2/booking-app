import { useNavigate } from "react-router-dom";
import "./DetailContent.css";

const DetailContent = (props) => {
  const navigate = useNavigate();

  const clickBookingHandle = () => {
    navigate(`/booking/${props.data._id}`);
  };
  return (
    <div className="details-content">
      <h2>{props.data.name}</h2>
      <p>{props.data.address}</p>
      <div className="distance-detail">{`Excellent location - ${props.data.distance}m from center`}</div>
      <div className="price-detail">{`Book a stay over $${props.data.cheapestPrice} at this property and get a free airport taxi`}</div>
      <div className="detail-pictures">
        <img src={props.data.photos[0]} alt="" />
        <img src={props.data.photos[1]} alt="" />
        <img src={props.data.photos[2]} alt="" />
        <img src={props.data.photos[3]} alt="" />
        <img src={props.data.photos[4]} alt="" />
        <img src={props.data.photos[5]} alt="" />
      </div>
      <div className="footer-details-content">
        <div className="decription-text">
          <h3>{props.data.title}</h3>
          <p>{props.data.desc}</p>
        </div>
        <div className="ads-blockk">
          <div className="price-ads">
            <strong>$ {props.data.cheapestPrice}</strong>
            <span> (1 nights)</span>
          </div>
          <button className="reverse-btn-ads" onClick={clickBookingHandle}>
            Reserve or Book Now!
          </button>
        </div>
      </div>
      {/* <button className="reverse-btn">Reserve or Book Now!</button> */}
    </div>
  );
};

export default DetailContent;
