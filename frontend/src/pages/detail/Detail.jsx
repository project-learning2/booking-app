import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

import SubscribeForm from "../../components/SubscribeForm/SubscribeForm";
import DetailContent from "./DetailContent/DetailContent";
import axios from "axios";

const Detail = () => {
  const [data, setData] = useState(null);
  const params = useParams().hotelId;

  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-hotel-by-id?hotelId=${params}`)
      .then((response) => setData(response.data))
      .catch((err) => console.log(err));
  }, [params]);
  return (
    <div>
      {data && <DetailContent data={data} />}
      <SubscribeForm />
    </div>
  );
};

export default Detail;
