import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import styles from "./Transactions.module.css";

const Transactions = () => {
  const userLogin = useSelector((state) => state.auth.userLogin);
  const navigate = useNavigate();
  const params = useParams().userId;
  const [data, setData] = useState(null);
  useEffect(() => {
    if (!userLogin) {
      navigate("/login");
    }
  }, [userLogin]);
  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-transactions?userId=${params}`)
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, [params]);

  const content = (
    <table>
      <thead>
        <tr>
          <th>#</th>
          <th>Hotel</th>
          <th>Room</th>
          <th>Date</th>
          <th>Price</th>
          <th>Payment Method</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {data &&
          data.map((el, index) => {
            const booked = <div className={styles.booked}>Booked</div>;
            const checkin = <div className={styles.checkin}>Checkin</div>;
            const checkout = <div className={styles.checkout}>Checkout</div>;
            let status;
            if (el.status === "Booked") {
              status = booked;
            }
            if (el.status === "Checkin") {
              status = checkin;
            }
            if (el.status === "Checkout") {
              status = checkout;
            }
            const room = el.room
              .map((roomNumber) => roomNumber.roomNumber)
              .join(", ");
            const dateStart = new Date(el.date.dateStart);
            const dateEnd = new Date(el.date.dateEnd);
            const date =
              dateStart.getDate() +
              "/" +
              dateStart.getMonth() +
              "/" +
              dateStart.getFullYear() +
              " - " +
              dateEnd.getDate() +
              "/" +
              dateEnd.getMonth() +
              "/" +
              dateEnd.getFullYear();
            return (
              <tr key={el._id}>
                <td>{index + 1}</td>
                <td>{el.hotel.name}</td>
                <td>{room}</td>
                <td>{date}</td>
                <td>${el.price}</td>
                <td>{el.payment}</td>
                <td>{status}</td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
  return (
    <div className={styles.transactions}>
      <h2>Your Transactions</h2>
      {userLogin ? content : <h1>Please Login</h1>}
    </div>
  );
};

export default Transactions;
