import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

import styles from "./Login.module.css";
import { authActions } from "../../store/auth";

const Login = () => {
  const [enteredUsername, setEnteredUsername] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onChangeUsernameHandle = (event) => {
    setEnteredUsername(event.target.value);
  };
  const onChangePasswordHanle = (event) => {
    setEnteredPassword(event.target.value);
  };
  const submitHandle = (event) => {
    setMessage("");
    event.preventDefault();
    axios
      .post("http://localhost:5000/login-user", {
        username: enteredUsername,
        password: enteredPassword,
      })
      .then((response) => {
        alert(response.data.message);
        dispatch(authActions.login(response.data.user));
        navigate("/");
      })
      .catch((err) => setMessage(err.response.data));
  };

  return (
    <div className={styles.login}>
      <h2>Login</h2>
      <form className={styles["login-form"]} onSubmit={submitHandle}>
        <p className={styles.message}>{message}</p>
        <input
          placeholder="username"
          onChange={onChangeUsernameHandle}
          type="text"
          value={enteredUsername}
        />
        <input
          placeholder="password"
          onChange={onChangePasswordHanle}
          type="password"
          value={enteredPassword}
        />
        <button type="submit">Login</button>
      </form>
    </div>
  );
};

export default Login;
