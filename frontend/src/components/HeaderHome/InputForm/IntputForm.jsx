import axios from "axios";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import BookingDateRange from "../../UI/DateRange";
import { searchActions } from "../../../store/resultSearch";

import styles from "./InputForm.module.css";
const InputForm = () => {
  const [enteredLocation, setEnteredLocation] = useState("");
  const [numberOfAdult, setnumberOfAdult] = useState(2);
  const [numberOfChildren, setnumberOfChildren] = useState(0);
  const [numberOfRoom, setnumberOfRoom] = useState(1);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [selectionRange, setSelectionRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: "selection",
  });
  const GetDateRangeInput = (value) => {
    setSelectionRange(value);
  };
  const startDate = selectionRange.startDate.toLocaleDateString("en-US", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const endDate = selectionRange.endDate.toLocaleDateString("en-US", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const [showCalendar, setShowCalendar] = useState(false);

  const ShowCelendarHandler = (events) => {
    events.preventDefault();
    showCalendar ? setShowCalendar(false) : setShowCalendar(true);
  };
  const locationOnChangeHandle = (events) => {
    setEnteredLocation(events.target.value);
  };
  const numberOfAdultChangeHanle = (events) => {
    setnumberOfAdult(events.target.value);
  };
  const numberOfChildrenChangeHandle = (events) => {
    setnumberOfChildren(events.target.value);
  };
  const numberOfRoomChangeHandle = (events) => {
    setnumberOfRoom(events.target.value);
  };
  const submitHandle = (events) => {
    events.preventDefault();
    axios
      .post("http://localhost:5000/search", {
        location: enteredLocation,
        people: Number(numberOfAdult) + Number(numberOfChildren),
        rooms: numberOfRoom,
        date: selectionRange,
      })
      .then((response) => {
        dispatch(searchActions.set(response.data));
        navigate("/search");
      })
      .catch((err) => console.log(err));
  };

  return (
    <form className={styles["input-infor-form"]}>
      <div className={styles["location-to-going"]}>
        <i className="fa fa-bed"></i>
        <input
          type="text"
          value={enteredLocation}
          placeholder=" where are you going?"
          onChange={locationOnChangeHandle}
        />
      </div>
      <div className={styles["date-booking"]}>
        <div className={showCalendar ? styles["celendar"] : styles["hide"]}>
          <BookingDateRange
            onHandlerGetDateRange={GetDateRangeInput}
            onStatusCelendar={setShowCalendar}
          />
        </div>
        <i className="fa fa-calendar"></i>
        <button onClick={ShowCelendarHandler}>{startDate}</button>
        <span> to </span>
        <button onClick={ShowCelendarHandler}>{endDate}</button>
      </div>
      <>
        <div className={styles["number-people-booking"]}>
          <span>
            <i className="fa fa-female"></i>
          </span>
          <div className={styles.number}>
            <input
              max="10"
              min="0"
              value={numberOfAdult}
              onChange={numberOfAdultChangeHanle}
              type="number"
            />
            <div>Adults</div>
          </div>
          <span> · </span>
          <div className={styles.number}>
            <input
              max="10"
              min="0"
              value={numberOfChildren}
              onChange={numberOfChildrenChangeHandle}
              type="number"
            />
            <div>children</div>
          </div>
          <span> · </span>
          <div className={styles.number}>
            <input
              max="10"
              min="0"
              value={numberOfRoom}
              onChange={numberOfRoomChangeHandle}
              type="number"
            />
            <div>room</div>
          </div>
        </div>
      </>

      <button
        type="submit"
        onClick={submitHandle}
        className={`${styles["btn-search"]} btn-search`}
      >
        Search
      </button>
    </form>
  );
};
export default InputForm;
