import ButtonUI from "../../UI/ButtonUI";
import styles from "./IntroPage.module.css";

const IntroPage = () => {
  return (
    <div className={styles["intro-paragrah"]}>
      <h1>A life of discounts? It's Genius.</h1>
      <p>
        Get rewarded for your travels-unlock instand savings of 10% or more with
        a free account
      </p>
      <button>Sign in/Register</button>
    </div>
  );
};

export default IntroPage;
