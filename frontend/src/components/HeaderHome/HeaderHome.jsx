import styles from "./HeaderHome.module.css";
import InputForm from "./InputForm/IntputForm";
import IntroPage from "./IntroPage/Intropage";

const HeaderHome = () => {
  return (
    <div className={styles["header-home"]}>
      <IntroPage />
      <InputForm />
    </div>
  );
};

export default HeaderHome;
