import "./TypeOfHotel.css";
import HotelComponent from "./HotelComponent";
// import data from "../../../assets/data/type.json";
import imgType0 from "../../../assets/images/type_1.webp";
import imgType1 from "../../../assets/images/type_2.jpg";
import imgType2 from "../../../assets/images/type_3.jpg";
import imgType3 from "../../../assets/images/type_4.jpg";
import imgType4 from "../../../assets/images/type_5.jpg";
import axios from "axios";
import { useEffect, useState } from "react";

const TypeOfHotel = () => {
  const [data, setData] = useState(null);
  useEffect(() => {
    axios
      .get("http://localhost:5000/get-hotel-by-type")
      .then((response) => {
        setData([
          {
            name: "Hotel",
            count: response.data.hotel.length,
          },
          {
            name: "Aparments",
            count: response.data.apartments.length,
          },
          {
            name: "Hotel",
            count: response.data.resorts.length,
          },
          {
            name: "Hotel",
            count: response.data.villas.length,
          },
          {
            name: "Hotel",
            count: response.data.cabins.length,
          },
        ]);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className="type-of-hotel">
      <h1>Browse by property type</h1>
      <div className="type-components">
        {data ? (
          <>
            <HotelComponent typeData={data[0]} imgType={imgType0} />
            <HotelComponent typeData={data[1]} imgType={imgType1} />
            <HotelComponent typeData={data[2]} imgType={imgType2} />
            <HotelComponent typeData={data[3]} imgType={imgType3} />
            <HotelComponent typeData={data[4]} imgType={imgType4} />
          </>
        ) : (
          <p>Loadingg......</p>
        )}
      </div>
    </div>
  );
};

export default TypeOfHotel;
