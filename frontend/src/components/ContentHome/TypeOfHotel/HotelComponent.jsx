import styles from "./HotelComponent.module.css";

const HotelComponent = (props) => {
  return (
    <div className={styles["hotel-component"]}>
      <div className={styles["img-type"]}>
        <img src={props.imgType} alt="" loading="lazy" />
      </div>
      <div className={styles["type-description"]}>
        <h2>{props.typeData.name}</h2>
        <h4>{`${props.typeData.count} hotels`}</h4>
      </div>
    </div>
  );
};

export default HotelComponent;
