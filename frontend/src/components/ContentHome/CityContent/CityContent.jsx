import axios from "axios";

import CityComponent from "./CityComponent/CityComponent";
import "./CityContent.css";
import imgCity1 from "../../../assets/images/HaNoi.jpg";
import imgCity2 from "../../../assets/images/HCM.jpg";
import imgCity3 from "../../../assets/images/DaNang.jpg";
import { useEffect, useState } from "react";

const CityContent = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-hotel-by-area`)
      .then((response) => {
        setData([
          {
            name: "Hà Nội",
            subText: response.data.HN.length + " properties",
          },
          {
            name: "Hồ Chí Minh",
            subText: response.data.HCM.length + " properties",
          },
          {
            name: "Đà Nẵng",
            subText: response.data.DN.length + " properties",
          },
        ]);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className="city-content">
      {data.length > 0 ? (
        <>
          <CityComponent cityData={data[0]} img={imgCity1} />
          <CityComponent cityData={data[1]} img={imgCity2} />
          <CityComponent cityData={data[2]} img={imgCity3} />
        </>
      ) : (
        <p>Loadinggggg...</p>
      )}
    </div>
  );
};

export default CityContent;
