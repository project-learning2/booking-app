import styles from "./CityComponent.module.css";

const CityComponent = (props) => {
  return (
    <div className={styles["city-component"]}>
      <div className={styles["img-city"]}>
        <img src={props.img} alt="test" />
      </div>
      <div className={styles["text-description"]}>
        <h2>{props.cityData.name}</h2>
        <h4>{props.cityData.subText}</h4>
      </div>
    </div>
  );
};

export default CityComponent;
