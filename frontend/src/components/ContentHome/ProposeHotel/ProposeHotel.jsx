import "./ProposeHotel.css";
import ProposeHotelComponent from "./ProposeHotelComponent";
import { useEffect, useState } from "react";
import axios from "axios";

const ProposeHotel = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get("http://localhost:5000/get-hotel-featured")
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className="propose-hotel">
      <h1>Homes guests love</h1>
      <div className="propose-hotel-components">
        {data.length > 0 ? (
          <>
            <ProposeHotelComponent
              hotelData={data[0]}
              img={data[0].photos[0]}
            />
            <ProposeHotelComponent
              hotelData={data[1]}
              img={data[1].photos[0]}
            />
            <ProposeHotelComponent
              hotelData={data[2]}
              img={data[2].photos[3]}
            />
          </>
        ) : (
          <p>Loading.........</p>
        )}
      </div>
    </div>
  );
};

export default ProposeHotel;
