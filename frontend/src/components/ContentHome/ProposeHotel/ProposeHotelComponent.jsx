import { Link } from "react-router-dom";
import styles from "./ProposeHotelComponent.module.css";

const ProposeHotelComponent = (props) => {
  return (
    <div className={styles["propose-hotel-component"]}>
      <div className={styles["img-propose-hotel"]}>
        <img src={props.img} alt="" />
      </div>
      <div className={styles["infor-hotel"]}>
        <Link to={`/detail/${props.hotelData._id}`}>
          <h2>{props.hotelData.name}</h2>
        </Link>
        <h5>{props.hotelData.city}</h5>
        <h4>{`Starting from $${props.hotelData.cheapestPrice}`}</h4>
        {/* <div className={styles["evalution-hotel"]}>
          <span
            className={styles["evalution-rate"]}
          >{`${props.hotelData.rate}`}</span>
          <span className={styles["evalution-type"]}>
            {props.hotelData.type}
          </span>
        </div> */}
      </div>
    </div>
  );
};

export default ProposeHotelComponent;
