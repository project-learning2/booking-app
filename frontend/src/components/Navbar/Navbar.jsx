import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import styles from "./Navbar.module.css";
import { authActions } from "../../store/auth";

const Navbar = () => {
  const navigate = useNavigate();
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const dispatch = useDispatch();

  const loginHande = () => {
    navigate("/login");
  };
  const registerHandle = () => {
    navigate("/register");
  };
  const logoutHandle = () => {
    dispatch(authActions.logout());
  };
  const clickHomePageHandle = () => {
    navigate("/");
  };
  const navTransactionsHanle = () => {
    navigate(`/transactions/${userLogin._id}`);
  };

  return (
    <div className={styles["nav-bar"]}>
      <div className={styles.content}>
        <div className={styles["above-nav"]}>
          <h2 onClick={clickHomePageHandle}>Booking Website</h2>
          {isAuth ? (
            <div className={styles["authenticated"]}>
              <div>{userLogin.username}</div>
              <button onClick={navTransactionsHanle}>Transactions</button>
              <button onClick={logoutHandle}>Logout</button>
            </div>
          ) : (
            <div className={styles["auth-container"]}>
              <button onClick={registerHandle}>Register</button>
              <button onClick={loginHande}>Login</button>
            </div>
          )}
        </div>
        <ul className={styles["nav-btns"]}>
          <li className={styles.active}>
            <Link href="https://www.w3schools.com/css/css_navbar.asp">
              <i className="fa fa-bed"></i>
              <span> Stay</span>
            </Link>
          </li>
          <li>
            <Link href="https://www.w3schools.com/css/css_navbar.asp">
              <i className="fa fa-plane"></i>
              <span> Flights</span>
            </Link>
          </li>
          <li>
            <Link href="https://www.w3schools.com/css/css_navbar.asp">
              <i className="fa fa-car"></i>
              <span> Car rentals</span>
            </Link>
          </li>
          <li>
            <Link href="https://www.w3schools.com/css/css_navbar.asp">
              <i className="fa fa-bed"></i>
              <span> Attractions</span>
            </Link>
          </li>
          <li>
            <Link href="https://www.w3schools.com/css/css_navbar.asp">
              <i className="fa fa-taxi"></i>
              <span> Airport taxis</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
