import ButtonUI from "../UI/ButtonUI";
import styles from "./SubscribeForm.module.css";
const SubscribeForm = () => {
  return (
    <div className={styles["subscribe-form"]}>
      <div className={styles["content-subscribe-form"]}>
        <h1>Save time, save money!</h1>
        <h4>Sign up and we'll send the best deals to you</h4>
        <div className={styles["input-form"]}>
          <input type="email" placeholder="Your Email" />
          <ButtonUI content={"Subscribe"} />
        </div>
      </div>
    </div>
  );
};

export default SubscribeForm;
