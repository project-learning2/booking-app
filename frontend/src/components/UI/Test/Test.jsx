import React, { useState } from "react";
import DateRange from "react-date-range";

const Example = () => {
  const [selectedRange, setSelectedRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
  });

  const handleSelect = (range) => {
    setSelectedRange(range);
  };

  return (
    <div>
      <DateRange onChange={handleSelect} selection={selectedRange} />
      <p>
        Selected Range: {selectedRange.startDate.toDateString()} -{" "}
        {selectedRange.endDate.toDateString()}
      </p>
    </div>
  );
};

export default Example;
