import React, { useState } from "react";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import { DateRange } from "react-date-range";

import styles from "./DateRange.module.css";
const BookingDateRange = (props) => {
  const [selectionRange, setSelectionRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: "selection",
  });
  //   const [showCalendar, setShowCalendar] = useState(props.onStatusCelendar);

  const handleSelect = (ranges) => {
    setSelectionRange({
      startDate: ranges.selection.startDate,
      endDate: ranges.selection.endDate,
      key: "selection",
    });
    props.onHandlerGetDateRange({
      startDate: ranges.selection.startDate,
      endDate: ranges.selection.endDate,
      key: "selection",
    });
    // props.onStatusCelendar(false);
  };
  const startDate = selectionRange.startDate.toLocaleDateString("en-US", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const endDate = selectionRange.endDate.toLocaleDateString("en-US", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });

  return (
    <div className={styles["date-range-component"]}>
      <DateRange
        editableDateInputs={true}
        moveRangeOnFirstSelection={true}
        className="date"
        minDate={new Date()}
        onChange={handleSelect}
        ranges={[selectionRange]}
        retainEndDateOnFirstSelection={true}
        showDateDisplay={true}
      />
    </div>
  );
};

export default BookingDateRange;
