import styles from "./ButtonUI.module.css";
const ButtonUI = (props) => {
  return <button className={styles.button}>{props.content}</button>;
};
export default ButtonUI;
