import { createBrowserRouter, RouterProvider } from "react-router-dom";

import Home from "./pages/home/Home";
import RootLayout from "./pages/RootLayout/RootLayout";
import Error from "./pages/Error/Error";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Detail from "./pages/detail/Detail";
import Search from "./pages/search/Search";
import Booking from "./pages/Booking/Booking";
import Transactions from "./pages/Transactions/Transactions";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <Error />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/register",
        element: <Register />,
      },
      {
        path: "/search",
        element: <Search />,
      },
      {
        path: "/detail/:hotelId",
        element: <Detail />,
      },
      {
        path: "/booking/:hotelBook",
        element: <Booking />,
      },
      {
        path: "/transactions/:userId",
        element: <Transactions />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
