import { configureStore } from "@reduxjs/toolkit";

import authReducer from "./auth";
import transactionReducer from "./transactions";
import searchReducer from "./resultSearch";

const store = configureStore({
  reducer: {
    auth: authReducer,
    transaction: transactionReducer,
    search: searchReducer,
  },
});

export default store;
