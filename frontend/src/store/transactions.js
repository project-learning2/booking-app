import { createSlice } from "@reduxjs/toolkit";

const initialTransactionsState = {
  transactiondata: null,
};

const transactionSlice = createSlice({
  name: "transaction",
  initialState: initialTransactionsState,
  reducers: {
    saveDataHanle(state, action) {
      state.transactiondata = action.payload;
    },
  },
});

export const transactionActions = transactionSlice.actions;

export default transactionSlice.reducer;
