import { createSlice } from "@reduxjs/toolkit";

const initialResultSearch = {
  result: [],
};

const resultSearchSlice = createSlice({
  name: "resultSearchSlice",
  initialState: initialResultSearch,
  reducers: {
    set(state, action) {
      state.result = action.payload;
    },
  },
});

export const searchActions = resultSearchSlice.actions;

export default resultSearchSlice.reducer;
