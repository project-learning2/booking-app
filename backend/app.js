// set-up
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

const app = express();

const adminRoutes = require("./routes/admin");
const bookingRoutes = require("./routes/booking");

app.use(cors());
app.use(bodyParser.json());
// routes
app.use("/", bookingRoutes);
app.use("/admin", adminRoutes);

// Connect database
mongoose
  .connect(
    "mongodb+srv://locnguyenddbrvt:Aas123123@cluster0.jyxohlo.mongodb.net/BookkingApp?retryWrites=true&w=majority"
  )
  .then((result) => {
    // User.findOne().then((user) => {
    //   if (!user) {
    //     const user = new User({
    //       name: "Max",
    //       email: "max@test.com",
    //       cart: {
    //         items: [],
    //       },
    //     });
    //     user.save();
    //   }
    // });
    app.listen(5000);
    console.log("Connected to Mongo DB");
  })
  .catch((err) => {
    console.log(err);
  });
