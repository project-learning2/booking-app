const express = require("express");

const router = express.Router();

const bookingController = require("../controllers/booking");

//
router.post("/register-user", bookingController.postCreatUser);
router.post("/login-user", bookingController.postLoginUser);
router.get("/get-hotel-by-area", bookingController.getHotelByArea);
router.get("/get-hotel-by-type", bookingController.getHotelByType);
router.get("/get-hotel-featured", bookingController.getHotelFeatured);
router.post("/search", bookingController.postSearchHotel);
router.get("/get-hotel-by-id", bookingController.getHotelById);
router.get("/get-room-by-id", bookingController.getRoomById);
router.post(
  "/post-create-transaction",
  bookingController.postCreateTransaction
);
router.get("/get-transactions", bookingController.getTransactions);

module.exports = router;
