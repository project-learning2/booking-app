const express = require("express");

const router = express.Router();

const adminController = require("../controllers/admin");

router.post("/login", adminController.postLogin);
router.get("/get-transactions", adminController.getAllTransactions);
router.get("/get-all-hotels", adminController.getAllHotels);
router.post("/delete-hotel", adminController.deleteHotel);
router.post("/create-hotel", adminController.createHotel);
router.get("/get-rooms", adminController.getRooms);
router.post("/delete-room", adminController.deleteRoom);
router.post("/create-room", adminController.postCreateRoom);
router.get("/get-all-transactions", adminController.getAllTransactionsss);
router.get("/get-hotel-by-Id", adminController.getHotelById);
router.post("/edit-hotel", adminController.editHotel);
router.get("/get-room-by-id", adminController.getRoomById);
router.post("/edit-room", adminController.postEditRoom);

module.exports = router;
