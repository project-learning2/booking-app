const ObjectId = require("mongodb").ObjectId;
const User = require("../models/user");
const Hotel = require("../models/hotel");
const Room = require("../models/room");
const Transaction = require("../models/transaction");

exports.postLogin = (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  User.findOne({ username: username }).then((result) => {
    if (result) {
      if (result.password === password) {
        if (result.isAdmin) {
          res.status(200).json({
            message: "Login success!!",
            user: result,
          });
        } else {
          res.status(401).json("This's not admin account!!");
        }
      } else {
        res.status(401).json("password is correct!!");
      }
    } else {
      res.status(401).json("username is correct!!");
    }
  });
};

exports.getAllTransactions = (req, res, next) => {
  Transaction.find()
    .populate("hotel")
    .populate("user")
    .exec()
    .then((result) => {
      const resultRes = result.reverse().slice(0, 8);
      res.status(200).json(resultRes);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getAllTransactionsss = (req, res, next) => {
  Transaction.find()
    .populate("hotel")
    .populate("user")
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getAllHotels = (req, res, next) => {
  Hotel.find().then((hotels) => {
    res.status(200).json(hotels);
  });
};

exports.deleteHotel = (req, res, next) => {
  const hotelId = req.query.hotelId;
  Transaction.find({ hotel: hotelId })
    .then((result) => {
      if (result.length === 0) {
        Hotel.deleteOne({ _id: hotelId })
          .then((result) => {
            res.status(201).json("Delete Success!!");
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        res.status(401).json("This hotel have order now1!!");
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.createHotel = (req, res, next) => {
  const name = req.body.name;
  const type = req.body.type;
  const city = req.body.city;
  const address = req.body.address;
  const distance = req.body.distance;
  const photos = req.body.urlImg;
  const decription = req.body.description;
  const featured = req.body.featured;
  // const rooms = req.body.rooms;
  const title = req.body.title;
  const price = req.body.price;

  const newRoom1 = new Room({
    title: "String",
    price: 300,
    maxPeople: 3,
    desc: "String",
    roomNumbers: ["101"],
  });
  const newRoom2 = new Room({
    title: "String",
    price: 400,
    maxPeople: 4,
    desc: "String",
    roomNumbers: ["101"],
  });
  const newHotel = new Hotel({
    name: name,
    city: city,
    type: type,
    title: title,
    address: address,
    distance: Number(distance),
    photos: [].push(photos),
    desc: decription,
    rating: 5,
    featured: featured === "true" ? true : false,
    rooms: ["6310dd998cfecfd90b30ca28"],
    cheapestPrice: Number(price),
  });
  newHotel
    .save()
    .then(() => {
      res.status(201).json("Create new hotel sucess!");
    })
    .catch((err) => {
      res.status(400).json(err._message);
    });
};

exports.getRooms = (req, res, next) => {
  Room.find().then((result) => {
    res.status(200).json(result);
  });
};

exports.deleteRoom = (req, res, next) => {
  const roomId = req.query.roomId;
  Transaction.find({ "room.room": roomId })
    .populate("room.room")
    .exec()
    .then((result) => {
      if (result.length === 0) {
        Room.deleteOne({ _id: roomId })
          .then(() => {
            res.status(201).json("Delete Success!!");
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        res.status(401).json("This room have order now1!!");
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postCreateRoom = (req, res, next) => {
  const title = req.body.title;
  const price = req.body.price;
  const maxPeople = req.body.maxPeople;
  const desc = req.body.description;
  const roomNumber = req.body.rooms.split(", ");
  const hotel = req.body.hotel;
  const room = new Room({
    title: title,
    price: Number(price),
    maxPeople: Number(maxPeople),
    desc: desc,
    roomNumbers: roomNumber,
  });
  room.save().then(() => {
    res.status(201).json("Create room success");
  });
};

exports.getHotelById = (req, res, next) => {
  const hotelId = req.query.hotelId;
  Hotel.findById(hotelId)
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.editHotel = (req, res, next) => {
  const hotelId = req.query.hotelId;
  console.log(hotelId);
  console.log(req.body);
  const name = req.body.name;
  const type = req.body.type;
  const city = req.body.city;
  const address = req.body.address;
  const distance = req.body.distance;
  const photos = req.body.urlImg;
  const description = req.body.description;
  const featured = req.body.featured;
  // const rooms = req.body.rooms;
  const title = req.body.title;
  const price = req.body.price;
  Hotel.findByIdAndUpdate(
    hotelId,
    {
      name: name,
      type: type,
      city: city,
      address: address,
      distance: distance,
      photos: photos,
      desc: description,
      featured: featured,
      cheapestPrice: price,
      title: title,
    },
    { new: true }
  )
    .then((updatedHotel) => {
      // console.log(updatedHotel);
      res.status(201).json("updatedHotel Success!!");
    })
    .catch((err) => {
      // console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.getRoomById = (req, res, next) => {
  const roomId = req.query.roomId;
  Room.findById(roomId)
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
};

exports.postEditRoom = (req, res, next) => {
  const roomId = req.query.roomId;
  const title = req.body.title;
  const price = req.body.price;
  const maxPeople = req.body.maxPeople;
  const desc = req.body.description;
  const roomNumber = req.body.rooms.split(", ");
  const hotel = req.body.hotel;
  Room.findByIdAndUpdate(
    roomId,
    {
      title: title,
      price: Number(price),
      maxPeople: Number(maxPeople),
      desc: desc,
      roomNumber,
    },
    { new: true }
  )
    .then((updatedHotel) => {
      res.status(201).json("updated Room Success!!");
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
};
