const User = require("../models/user");
const Hotel = require("../models/hotel");
const Room = require("../models/room");
const Transaction = require("../models/transaction");
const getDb = require("../utils/database").getDb;
const ObjectId = require("mongodb").ObjectId;
const mongoose = require("mongoose");

// /register-user => POST
exports.postCreatUser = (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const fullName = req.body.fullName;
  const phoneNumber = req.body.phoneNumber;
  const email = req.body.email;
  const newUser = new User({
    username: username,
    password: password,
    fullName: fullName,
    phoneNumber: phoneNumber,
    email: email,
  });
  newUser
    .save()
    .then(() => {
      res.status(201).json("Create user sucesss!!");
    })
    .catch((err) => {
      res.status(400).json("Username's exits!!");
    });
};

// /login-user => POST
exports.postLoginUser = (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  User.findOne({ username: username }).then((result) => {
    if (result) {
      if (result.password === password) {
        res.status(200).json({
          message: "Login sucsess",
          user: result,
        });
      } else {
        res.status(404).json("Correct password!!");
      }
    } else {
      res.status(404).json("username not exits");
    }
  });
};

exports.getHotelByArea = (req, res, next) => {
  const promises = [
    Hotel.find({ city: "Ho Chi Minh" }),
    Hotel.find({ city: "Da Nang" }),
    Hotel.find({ city: "Ha Noi" }),
  ];

  Promise.all(promises)
    .then((results) => {
      const hcmHotels = results[0];
      const danangHotels = results[1];
      const hanoiHotels = results[2];
      res.status(200).json({
        HCM: hcmHotels,
        DN: danangHotels,
        HN: hanoiHotels,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getHotelByType = (req, res, next) => {
  const promises = [
    Hotel.find({ type: "hotel" }),
    Hotel.find({ type: "apartments" }),
    Hotel.find({ type: "resorts" }),
    Hotel.find({ type: "villas" }),
    Hotel.find({ type: "cabins" }),
  ];
  Promise.all(promises)
    .then((result) => {
      const hotel = result[0];
      const apartments = result[1];
      const resorts = result[2];
      const villas = result[3];
      const cabins = result[4];
      res.status(200).json({
        hotel: hotel,
        apartments: apartments,
        resorts: resorts,
        villas: villas,
        cabins: cabins,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Vì không thấy thuộc tính rating nên dùng featured
exports.getHotelFeatured = (req, res, next) => {
  Hotel.find({ featured: true })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
    });
};
//  /search => POST
exports.postSearchHotel = (req, res, next) => {
  const location = req.body.location.trim().toUpperCase();
  const people = req.body.people;
  const rooms = req.body.rooms;
  Hotel.find()
    .then((hotels) => {
      const locationSearch = hotels.filter((hotel) =>
        hotel.city.trim().toUpperCase().includes(location)
      );
      const promises = [];
      locationSearch.forEach((el) => {
        const promise = Hotel.findById(el._id)
          .populate("rooms")
          .exec()
          .then((hotel) => {
            return {
              hotel: hotel,
              rooms: hotel.rooms,
            };
          })
          .catch((err) => {
            console.log(err);
          });
        promises.push(promise);
      });
      Promise.all(promises)
        .then((result) => {
          const resultResponse = [];
          result.forEach((el) => {
            const roomss = el.rooms;
            let totalRoom = 0;
            let totalPeople = 0;
            roomss.forEach((roomEl) => {
              totalRoom += roomEl.roomNumbers.length;
              totalPeople += Number(roomEl.maxPeople);
            });
            if (Number(rooms) <= totalRoom && Number(people) <= totalPeople) {
              resultResponse.push(el.hotel);
            }
          });
          res.status(200).json(resultResponse);
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getHotelById = (req, res, next) => {
  const hotelId = req.query.hotelId;
  Hotel.findById(hotelId).then((result) => {
    res.status(200).json(result);
  });
};

exports.getRoomById = (req, res, next) => {
  const hotelId = req.query.hotelId;
  Hotel.findById(hotelId)
    .populate("rooms")
    .exec()
    .then((result) => {
      res.status(200).json(result.rooms);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postCreateTransaction = (req, res, next) => {
  const date = req.body.date;
  const dateStart = new Date(date.dateStart);
  const dateEnd = new Date(date.dateEnd);
  const currentDate = new Date();
  let status;
  if (dateStart < currentDate && dateEnd > currentDate) {
    status = "Checkin";
  }
  if (dateStart < currentDate && dateEnd < currentDate) {
    status = "Checkout";
  }
  if (dateStart > currentDate && dateEnd > currentDate) {
    status = "Booked";
  }
  const room = req.body.room;
  const hotel = req.body.hotel;
  const price = req.body.price;
  const payment = req.body.paymentMethod;
  const user = req.body.userId;
  const newTransaction = new Transaction({
    user: user,
    hotel: hotel,
    room: room.map((el) => {
      return {
        room: el.room,
        roomNumber: el.roomNumber,
        price: el.price,
      };
    }),
    date: {
      dateStart: dateStart,
      dateEnd: dateEnd,
    },
    price: price,
    payment: payment,
    status: status,
  });
  newTransaction
    .save()
    .then(() => {
      res.status(201).json("Create transaction sucesss!!");
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json("Some thing went wrong!!!");
    });
};

exports.getTransactions = (req, res, next) => {
  const userId = req.query.userId;
  Transaction.find({ user: userId })
    .populate("hotel")
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
    });
};
