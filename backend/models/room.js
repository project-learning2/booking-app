const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomSchema = new Schema({
  title: String,
  price: Number,
  maxPeople: Number,
  desc: String,
  roomNumbers: [String],
  // hotel: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'Hotel'
  // }
});

module.exports = mongoose.model("Room", roomSchema);
