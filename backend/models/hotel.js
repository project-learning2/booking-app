const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// const roomSchema = new Schema({
//   title: { type: String, required: true },
//   price: { type: Number, required: true },
//   maxPeople: { type: Number, required: true },
//   desc: { type: String, required: true },
//   roomNumbers: [{ type: String, required: true }],
// });

const hotelSchema = new Schema({
  name: { type: String, required: true },
  type: { type: String, required: true },
  city: { type: String, required: true },
  address: { type: String, required: true },
  distance: { type: Number, required: true },
  photos: [{ type: String }],
  desc: { type: String, required: true },
  rating: { type: Number, required: true },
  featured: { type: Boolean, default: false },
  rooms: [{ type: Schema.Types.ObjectId, ref: "Room" }],
  cheapestPrice: { type: Number, required: true },
  title: { type: String, required: true },
});

module.exports = mongoose.model("Hotel", hotelSchema);
