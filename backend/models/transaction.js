const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  hotel: { type: Schema.Types.ObjectId, ref: "Hotel", required: true },
  room: [
    {
      room: {
        type: Schema.Types.ObjectId,
        ref: "Room",
        required: true,
      },
      roomNumber: { type: String, required: true },
      price: { type: Number, require: true },
    },
  ],
  date: {
    dateStart: { type: Date, required: true },
    dateEnd: { type: Date, required: true },
  },
  price: { type: Number, required: true },
  payment: { type: String, enum: ["Credit/Debit", "Cash"], required: true },
  status: {
    type: String,
    enum: ["Booked", "Checkin", "Checkout"],
    required: true,
  },
});

module.exports = mongoose.model("Transaction", transactionSchema);
