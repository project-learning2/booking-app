import { createBrowserRouter, RouterProvider } from "react-router-dom";
import AddNewHotel from "./pages/AddNewHotel/AddNewHotel";
import AddNewRoom from "./pages/AddNewRoom/AddNewRoom";
import DashBoard from "./pages/DashBoard/DashBoard";
import EditHotel from "./pages/EditHotel/EditHotel";
import EditRoom from "./pages/EditRoom/EditRoom";
import Hotel from "./pages/Hotel/Hotel";
import Login from "./pages/Login/Login";
import Room from "./pages/Room/Room";
import RootLayout from "./pages/RootLayout/RootLayout";
import Transactions from "./pages/Transaction/Transaction";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/dashboard",
        element: <DashBoard />,
      },
      {
        path: "/hotel",
        element: <Hotel />,
      },
      {
        path: "/add-new-hotel",
        element: <AddNewHotel />,
      },
      {
        path: "/room",
        element: <Room />,
      },
      {
        path: "/add-new-room",
        element: <AddNewRoom />,
      },
      {
        path: "/transaction",
        element: <Transactions />,
      },
      {
        path: "/edit-hotel/:hotelId",
        element: <EditHotel />,
      },
      {
        path: "/edit-room/:roomId",
        element: <EditRoom />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
