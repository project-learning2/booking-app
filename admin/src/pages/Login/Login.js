import axios from "axios";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import styles from "./Login.module.css";
import { authActions } from "../../store/auth";

const Login = () => {
  const [enteredUsername, setEnteredUsername] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [message, setMessage] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const changeUserNameHandle = (events) => {
    setEnteredUsername(events.target.value);
  };
  const changePasswordHandle = (events) => {
    setEnteredPassword(events.target.value);
  };
  const submitLogin = (events) => {
    events.preventDefault();
    setMessage(null);
    axios
      .post("http://localhost:5000/admin/login", {
        username: enteredUsername,
        password: enteredPassword,
      })
      .then((response) => {
        const user = response.data.user;
        if (user) {
          dispatch(authActions.login(user));
          navigate("/");
        }
      })
      .catch((err) => setMessage(err.response.data));
  };

  return (
    <div className={styles.login}>
      <h2>Login</h2>
      <form className={styles["login-form"]} onSubmit={submitLogin}>
        <div className={message ? styles.message : styles.hide}>{message}</div>
        <input
          placeholder="username"
          type="text"
          onChange={changeUserNameHandle}
          value={enteredUsername}
        />
        <input
          placeholder="password"
          type="password"
          onChange={changePasswordHandle}
          value={enteredPassword}
        />
        <button type="submit">Login</button>
      </form>
    </div>
  );
};

export default Login;
