import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import styles from "./Room.module.css";

const Room = () => {
  const [roomData, setRoomData] = useState(null);
  const navigate = useNavigate();

  const fetchRoomData = () => {
    axios
      .get("http://localhost:5000/admin/get-rooms")
      .then((response) => {
        setRoomData(response.data);
      })
      .catch((err) => console.log(err));
  };
  useEffect(() => {
    fetchRoomData();
  }, []);
  const deleteRoomHandle = (room) => {
    const confirm = window.confirm("You really want to delete??");
    if (confirm) {
      axios
        .post(`http://localhost:5000/admin/delete-room?roomId=${room._id}`)
        .then((response) => {
          alert(response.data);
          fetchRoomData();
        })
        .catch((err) => {
          alert(err.response.data);
        });
    }
  };
  const addNewHandle = () => {
    navigate("/add-new-room");
  };
  const editHandle = (roomId) => {
    navigate(`/edit-room/${roomId}`);
  };
  return (
    <div className={styles.room}>
      <h3>Rooms List</h3>
      <button className={styles["add-btn"]} onClick={addNewHandle}>
        Add New
      </button>
      <div className={styles.table}>
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>ID</th>
              <th>Title</th>
              <th>Description</th>
              <th>Price</th>
              <th>MaxPeople</th>
              <th>Action</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {roomData &&
              roomData.map((el) => {
                return (
                  <tr key={el._id} className={styles.el}>
                    <td>
                      <input type="checkbox" />
                    </td>
                    <td>{el._id}</td>
                    <td>{el.title}</td>
                    <td>{el.desc}</td>
                    <td>{el.price}</td>
                    <td>{el.maxPeople}</td>
                    <td>
                      <button onClick={() => deleteRoomHandle(el)}>
                        Delete
                      </button>
                      <button
                        style={{
                          color: "#179733",
                          border: "1px dashed #179733",
                          marginLeft: "5px",
                        }}
                        onClick={() => editHandle(el._id)}
                      >
                        Edit
                      </button>
                    </td>
                    <td></td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <div className={styles.page}>
          <div className={styles["page-display"]}>
            <p>1-8 of 8 </p>
            <div className={styles["page-actions"]}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <FontAwesomeIcon icon={faChevronRight} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Room;
