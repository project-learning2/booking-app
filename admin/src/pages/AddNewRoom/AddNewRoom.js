import { useState, useEffect } from "react";
import axios from "axios";

import styles from "./AddNewRoom.module.css";
import { useNavigate } from "react-router-dom";

const AddNewRoom = () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [maxPeople, setMaxPeople] = useState("");
  const [rooms, setRooms] = useState("");
  const [hotel, setHotel] = useState("");
  const [hotelsData, setHotelsData] = useState(null);
  const navigate = useNavigate();
  useEffect(() => {
    axios
      .get("http://localhost:5000/admin/get-all-hotels")
      .then((response) => {
        setHotelsData(response.data.map((el) => el.name));
      })
      .catch((err) => console.log(err));
  }, []);
  const changeTitle = (events) => {
    setTitle(events.target.value);
  };
  const changePrice = (events) => {
    setPrice(events.target.value);
  };
  const changeDescription = (events) => {
    setDescription(events.target.value);
  };
  const changeMaxPeople = (events) => {
    setMaxPeople(events.target.value);
  };
  const changeRooms = (events) => {
    setRooms(events.target.value);
  };
  const changeHotel = (events) => {
    setHotel(events.target.value);
  };
  const submitHandle = (events) => {
    events.preventDefault();
    const dataForm = {
      title: title,
      price: price,
      description: description,
      maxPeople: maxPeople,
      rooms: rooms,
      hotel: hotel,
    };
    console.log(dataForm);
    axios
      .post("http://localhost:5000/admin/create-room", dataForm)
      .then((response) => {
        alert(response.data);
        navigate("/room");
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className={styles["add-new-room"]}>
      <div className={styles.title}>
        <h3>Add New Product</h3>
      </div>
      <form className={styles.form} onSubmit={submitHandle}>
        <div className={styles.left}>
          <label>Title</label>
          <input type="text" onChange={changeTitle} value={title} />
          <label>Price</label>
          <input type="text" onChange={changePrice} value={price} />
          <label>Rooms</label>
          <textarea
            id="source-text"
            placeholder="Ex: 101, 102, 103,..."
            onChange={changeRooms}
            value={rooms}
          ></textarea>
        </div>
        <div className={styles.right}>
          <label>Description</label>
          <input type="text" onChange={changeDescription} value={description} />
          <label>MaxPeople</label>
          <input type="number" onChange={changeMaxPeople} value={maxPeople} />
          <label>Choose a hotel</label>
          <select onChange={changeHotel}>
            <option value="#">Choose Hotel...</option>
            {hotelsData &&
              hotelsData.map((el) => {
                return (
                  <option key={el} value={el}>
                    {el}
                  </option>
                );
              })}
          </select>
        </div>
        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default AddNewRoom;
