import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";

import styles from "./Transaction.module.css";

const Transactions = () => {
  const [data, setData] = useState(null);
  useEffect(() => {
    axios
      .get("http://localhost:5000/admin/get-all-transactions")
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className={styles.transaction}>
      <h3>Transaction List</h3>
      <div className={styles.table}>
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>ID</th>
              <th>User</th>
              <th>Hotel</th>
              <th>Room</th>
              <th>Date</th>
              <th>Price</th>
              <th>Payment Method</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((el) => {
                const dateStart = new Date(el.date.dateStart);
                const dateEnd = new Date(el.date.dateEnd);
                const date =
                  dateStart.getDate() +
                  "/" +
                  dateStart.getMonth() +
                  "/" +
                  dateStart.getFullYear() +
                  " - " +
                  dateEnd.getDate() +
                  "/" +
                  dateEnd.getMonth() +
                  "/" +
                  dateEnd.getFullYear();
                const booked = <div className={styles.booked}>Booked</div>;
                const checkin = <div className={styles.checkin}>Checkin</div>;
                const checkout = (
                  <div className={styles.checkout}>Checkout</div>
                );
                let status;
                if (el.status === "Booked") {
                  status = booked;
                }
                if (el.status === "Checkin") {
                  status = checkin;
                }
                if (el.status === "Checkout") {
                  status = checkout;
                }
                return (
                  <tr key={el._id}>
                    <td>
                      <input type="checkbox" />
                    </td>
                    <td>{el._id}</td>
                    <td>{el.user.fullName}</td>
                    <td>{el.hotel.name}</td>
                    <td>
                      {" "}
                      {el.room
                        .map((roomNumber) => roomNumber.roomNumber)
                        .join(", ")}
                    </td>
                    <td>{date}</td>
                    <td>${el.price}</td>
                    <td>{el.payment}</td>
                    <td>{status}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <div className={styles.page}>
          <div className={styles["page-display"]}>
            <p>1-8 of 8 </p>
            <div className={styles["page-actions"]}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <FontAwesomeIcon icon={faChevronRight} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Transactions;
