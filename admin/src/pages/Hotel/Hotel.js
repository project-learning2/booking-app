import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import styles from "./Hotel.module.css";

const Hotel = () => {
  const navigate = useNavigate();
  const [hotelsData, setHotelsData] = useState(null);
  const fetchAllHotels = () => {
    axios
      .get("http://localhost:5000/admin/get-all-hotels")
      .then((response) => {
        setHotelsData(response.data);
      })
      .catch((err) => console.log(err));
  };
  useEffect(() => {
    fetchAllHotels();
  }, []);

  const deleteHotelHandle = (hotel) => {
    const confirm = window.confirm("You really want to delete??");
    if (confirm) {
      axios
        .post(`http://localhost:5000/admin/delete-hotel?hotelId=${hotel._id}`)
        .then((response) => {
          alert(response.data);
          fetchAllHotels();
        })
        .catch((err) => {
          alert(err.response.data);
        });
    }
  };
  const addNewHandle = () => {
    navigate("/add-new-hotel");
  };
  const editHandle = (hotelId) => {
    navigate(`/edit-hotel/${hotelId}`);
  };
  return (
    <div className={styles.hotels}>
      <h3>Hotels List</h3>
      <button className={styles["add-btn"]} onClick={addNewHandle}>
        Add New
      </button>
      <div className={styles.table}>
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>ID</th>
              <th>Name</th>
              <th>Type</th>
              <th>Title</th>
              <th>City</th>
              <th>Action</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {hotelsData &&
              hotelsData.map((el) => {
                return (
                  <tr key={el._id} className={styles.hotel}>
                    <td>
                      <input type="checkbox" />
                    </td>
                    <td>{el._id}</td>
                    <td>{el.name}</td>
                    <td>{el.type}</td>
                    <td>{el.title}</td>
                    <td>{el.city}</td>
                    <td>
                      <button onClick={() => deleteHotelHandle(el)}>
                        Delete
                      </button>
                      <button
                        style={{
                          color: "#179733",
                          border: "1px dashed #179733",
                          marginLeft: "5px",
                        }}
                        onClick={() => editHandle(el._id)}
                      >
                        Edit
                      </button>
                    </td>
                    <td></td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <div className={styles.page}>
          <div className={styles["page-display"]}>
            <p>1-8 of 8 </p>
            <div className={styles["page-actions"]}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <FontAwesomeIcon icon={faChevronRight} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hotel;
