import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import styles from "./EditHotel.module.css";

const EditHotel = () => {
  const navigate = useNavigate();
  const params = useParams().hotelId;
  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [distance, setDistance] = useState("");
  const [description, setDescription] = useState("");
  const [type, setType] = useState("");
  const [address, setAddress] = useState("");
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [featured, setFeatured] = useState(false);
  const [rooms, setRooms] = useState("");
  const [urlImg, setUrlImg] = useState("");

  useEffect(() => {
    axios
      .get(`http://localhost:5000/admin/get-hotel-by-Id?hotelId=${params}`)
      .then((response) => {
        const data = response.data;
        setName(data.name);
        setCity(data.city);
        setDistance(data.distance);
        setDescription(data.desc);
        setUrlImg(data.photos[0]);
        setType(data.type);
        setAddress(data.address);
        setTitle(data.title);
        setPrice(data.cheapestPrice);
        setFeatured(data.featured);
      })
      .catch((err) => console.log(err));
  }, []);

  const changeName = (events) => {
    setName(events.target.value);
  };
  const changeCity = (events) => {
    setCity(events.target.value);
  };
  const changeDistance = (events) => {
    setDistance(events.target.value);
  };
  const changeType = (events) => {
    setType(events.target.value);
  };
  const changeAdress = (events) => {
    setAddress(events.target.value);
  };
  const changeTitle = (events) => {
    setTitle(events.target.value);
  };
  const changePrice = (events) => {
    setPrice(events.target.value);
  };
  const changeFeatured = (events) => {
    setFeatured(events.target.value);
  };
  const changeDescription = (events) => {
    setDescription(events.target.value);
  };
  const changeRooms = (events) => {
    setRooms(events.target.value);
  };
  const changeUrlImg = (events) => {
    setUrlImg(events.target.value);
  };
  const submitHanle = (events) => {
    events.preventDefault();
    const data = {
      name: name,
      city: city,
      distance: distance,
      description: description,
      urlImg: urlImg,
      rooms: rooms,
      type: type,
      address: address,
      title: title,
      price: price,
      featured: featured,
    };
    if (
      name.trim().length === 0 ||
      city.trim().length === 0 ||
      distance === "" ||
      description.trim().length === 0 ||
      urlImg.trim().length === 0 ||
      //   rooms.trim().length === 0 ||
      type.trim().length === 0 ||
      address.trim().length === 0 ||
      title.trim().length === 0 ||
      price === "" ||
      featured === ""
    ) {
      alert("Input Infor");
    } else {
      axios
        .post(`http://localhost:5000/admin/edit-hotel?hotelId=${params}`, data)
        .then((response) => {
          console.log(response);
          alert(response.data);
          navigate("/hotel");
        })
        .catch((err) => {
          alert(err.response.data);
          console.log(err);
        });
    }
  };

  return (
    <div className={styles["add-new-hotel"]}>
      <div className={styles.title}>
        <h3>Add New Product</h3>
      </div>
      <form className={styles.form} onSubmit={submitHanle}>
        <div className={styles.left}>
          <label>Name</label>
          <input type="text" onChange={changeName} value={name} />
          <label>City</label>
          <input type="text" onChange={changeCity} value={city} />
          <label>Distance from city center</label>
          <input type="number" onChange={changeDistance} value={distance} />
          <label>Description</label>
          <input type="text" onChange={changeDescription} value={description} />
          <label>Image</label>
          <input type="text" onChange={changeUrlImg} value={urlImg} />
        </div>
        <div className={styles.right}>
          <label>Type</label>
          <input type="text" onChange={changeType} value={type} />
          <label>Adress</label>
          <input type="text" onChange={changeAdress} value={address} />
          <label>Title</label>
          <input type="text" onChange={changeTitle} value={title} />
          <label>Price</label>
          <input type="number" onChange={changePrice} value={price} />
          <label>Featured</label>
          <select onChange={changeFeatured} value={featured}>
            <option value={true}>Yes</option>
            <option value={false}>No</option>
          </select>
        </div>
        <div className={styles["rooms-input"]}>
          <label>Rooms</label>
          <textarea
            id="source-text"
            placeholder="Input here rooms information"
            onChange={changeRooms}
            value={rooms}
          ></textarea>
        </div>
        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default EditHotel;
