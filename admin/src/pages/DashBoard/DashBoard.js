import { faUser } from "@fortawesome/free-regular-svg-icons";
import {
  faCartShopping,
  faChevronLeft,
  faChevronRight,
  faMoneyBill,
  faWallet,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import styles from "./DashBoard.module.css";

const DashBoard = () => {
  const [data, setData] = useState(null);
  useEffect(() => {
    axios
      .get("http://localhost:5000/admin/get-transactions")
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className={styles.dashboard}>
      <div className={styles["infor-board"]}>
        <div className={styles["board-el"]}>
          <h6>USERS</h6>
          <p>100</p>
          <div className={styles["board-icon"]}>
            <FontAwesomeIcon icon={faUser} />
          </div>
        </div>
        <div className={styles["board-el"]}>
          <h6>ORDERS</h6>
          <p>100</p>
          <div
            style={{
              color: "#aa8b00",
              backgroundColor: "#faedb1",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faCartShopping} />
          </div>
        </div>
        <div className={styles["board-el"]}>
          <h6>EARNINGS</h6>
          <p>100</p>
          <div
            style={{
              color: "#119200",
              backgroundColor: "#bffdb7",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faMoneyBill} />
          </div>
        </div>
        <div className={styles["board-el"]}>
          <h6>BALANCE</h6>
          <p>100</p>
          <div
            style={{
              color: "#8e009b",
              backgroundColor: "#f4c7ff",
            }}
            className={styles["board-icon"]}
          >
            <FontAwesomeIcon icon={faWallet} />
          </div>
        </div>
      </div>
      <div className={styles.transactions}>
        <h3>Lastest Transactions</h3>
        <div className={styles.table}>
          <table>
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>User</th>
                <th>Hotel</th>
                <th>Room</th>
                <th>Date</th>
                <th>Price</th>
                <th>Payment Method</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((el) => {
                  const dateStart = new Date(el.date.dateStart);
                  const dateEnd = new Date(el.date.dateEnd);
                  const date =
                    dateStart.getDate() +
                    "/" +
                    dateStart.getMonth() +
                    "/" +
                    dateStart.getFullYear() +
                    " - " +
                    dateEnd.getDate() +
                    "/" +
                    dateEnd.getMonth() +
                    "/" +
                    dateEnd.getFullYear();
                  const booked = <div className={styles.booked}>Booked</div>;
                  const checkin = <div className={styles.checkin}>Checkin</div>;
                  const checkout = (
                    <div className={styles.checkout}>Checkout</div>
                  );
                  let status;
                  if (el.status === "Booked") {
                    status = booked;
                  }
                  if (el.status === "Checkin") {
                    status = checkin;
                  }
                  if (el.status === "Checkout") {
                    status = checkout;
                  }
                  return (
                    <tr key={el._id}>
                      <td>
                        <input type="checkbox" />
                      </td>
                      <td>{el._id}</td>
                      <td>{el.user.fullName}</td>
                      <td>{el.hotel.name}</td>
                      <td>
                        {el.room
                          .map((roomNumber) => roomNumber.roomNumber)
                          .join(", ")}
                      </td>
                      <td>{date}</td>
                      <td>${el.price}</td>
                      <td>{el.payment}</td>
                      <td>{status}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
          <div className={styles.page}>
            <div className={styles["page-display"]}>
              <p>1-8 of 8 </p>
              <div className={styles["page-actions"]}>
                <FontAwesomeIcon icon={faChevronLeft} />
                <FontAwesomeIcon icon={faChevronRight} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashBoard;
