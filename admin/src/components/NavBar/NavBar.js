import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTableColumns,
  faHotel,
  faWhiskeyGlass,
  faRightLeft,
  faRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-regular-svg-icons";
import { authActions } from "../../store/auth";
import { useDispatch } from "react-redux";

import styles from "./NavBar.module.css";
const NavBar = () => {
  const dispatch = useDispatch();
  return (
    <header className={styles["nav-bar"]}>
      <nav className={styles.nav}>
        <ul>
          <li>
            <h4>MAIN</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faTableColumns} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/dashboard"
              >
                DashBoard
              </NavLink>
            </div>
          </li>
          <li>
            <h4>LIST</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faUser} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/test"
              >
                Users
              </NavLink>
            </div>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faHotel} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/hotel"
              >
                Hotels
              </NavLink>
            </div>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faWhiskeyGlass} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/room"
              >
                Rooms
              </NavLink>
            </div>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faRightLeft} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/transaction"
              >
                Transactions
              </NavLink>
            </div>
          </li>
          <li>
            <h4>NEW</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faHotel} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/add-new-hotel"
              >
                NewHotel
              </NavLink>
            </div>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faWhiskeyGlass} />
              <NavLink
                className={({ isActive }) =>
                  isActive ? styles.active : undefined
                }
                to="/add-new-room"
              >
                NewRoom
              </NavLink>
            </div>
          </li>
          <li>
            <h4>USER</h4>
            <div className={styles["nav-el"]}>
              <FontAwesomeIcon icon={faRightFromBracket} />
              <span
                onClick={() => {
                  dispatch(authActions.logout());
                }}
              >
                Logout
              </span>
            </div>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default NavBar;
